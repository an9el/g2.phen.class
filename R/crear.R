suppressPackageStartupMessages({
    library(pacman)
    p_load(R6, tidyverse, plyr, Hmisc, labelled, magrittr, stringr, glue, here, stringr, fs, checkmate)
})



source('gait2.fenotypes.class.R')
dataDir <- glue::glue("{here::here()}/data")
load(glue::glue("{dataDir}/gait2.phe.all.RData"))

rm(g2.ph.covariates,
   g2.ph.description,
   g2.ph.formula,
   g2.ph.incompatibilities,
   g2.ph.machine,
   g2.ph.normalRange,
   g2.ph.observations,
   g2.ph.units,
   fenos500,
   g2.ph.categories)


## problem with ABOa0, is wrong, here we correct it:
## df <- df %>% mutate(AB0aA = case_when(
## AB0 %in% Cs(O1/O1,O1/O2,B/O1,B/O2,B/B) ~ 0,
## AB0 %in% Cs(A2/O2,A2/O1,A2/B,A1/O2,A1/O1,A1/B) ~ 1,
## AB0 %in% Cs(A1/A1,A1/A2,A2/A2) ~ 2,
## TRUE ~ as.numeric(AB0)))
## df <- df %>% mutate(AB0aA = as.factor(AB0aA))

## df <- df %>% mutate(AB0a0 = case_when(
##  AB0 %in% Cs(O1/O1,O1/O2) ~ 2,
##  AB0 %in% Cs(A2/O2,A2/O1,A1/O2,A1/O1,B/O1,B/O2) ~ 1,
##  AB0 %in% Cs(A1/A1,A1/A2,A2/A2,B/B,A1/B,A2/B) ~ 0,
##  TRUE ~ as.numeric(AB0)))
##  df <- df %>% mutate(AB0a0 = as.factor(AB0a0))


## df <- df %>% mutate(AB0aA1 = case_when(
##       AB0 %in% Cs(A2/O2,A2/O1,A2/B,O1/O1,O1/O2,B/O1,B/O2,B/B,A2/A2) ~ 0,
##       AB0 %in% Cs(A1/O2,A1/A2,A1/O1,A1/B) ~ 1,
##       AB0 %in% Cs(A1/A1) ~ 2,
##       TRUE ~ as.numeric(AB0)))


source('g2.ph.categories.R')
source('gait2.fenotypes.class.R')
source('g2.ph.units.R')
source('g2.ph.normalRange.R')
source('g2.ph.machine.R')
source('g2.ph.formula.R')
source('g2.ph.description.R')
source('g2.ph.observations.R')
source('g2.ph.incompatibilities.R')
source('g2.ph.labels.R')
load(glue::glue("{dataDir}/g2.ph.covariates.500.gait2.RData"))
load(glue::glue("{dataDir}/g2.h2r.fenos500.gait2.RData"))
load(glue::glue("{dataDir}/g2.ph.map.prot.RData"))

## kk <- spread(g2.r.h2r.miRNA.fase2[,c('miRNA','covs')], key=miRNA,value=covs) %>% as.list
## kk2 <- lapply(kk, function(X) strsplit(X,split=',',fixed=TRUE)[[1]])

## df <- Hmisc::upData(df,
##                     units=unlist(g2.ph.units) %>% dput,
##                     labels = unlist(g2.ph.labels) %>% dput)

## k <- df[1:4,1:6]
## k2 <- Hmisc::upData(k,units=rep(NA,ncol(k)),labels=rep(NA,ncol(k)))
## This was a fuck up, because labelled class give a lot of problems.
## To delete this class, I have to do the following:
##   
##   buscarLabel <- function(X){any(class(X) == 'labelled')}
##   deleteLabel <- function(X){
##       clase <- setdiff(class(X), 'labelled')
##       cambiar <- function(f) f(X)
##       stopifnot(require(lubridate))
##       if(any(clase == 'ordered')){
##           X <- as.character(X)
##           X <- cambiar(as.ordered)
##       }else if(any(clase == 'POSIXct')){
##           X <- cambiar(as_date)
##       }else if(clase == 'character'){
##           X <- cambiar(as.character)
##       }else if(clase == 'factor'){
##           X <- as.character(X)
##           X <- cambiar(as.factor)
##       }else if(clase == 'integer'){
##           X <- cambiar(as.integer)
##       }else if(clase == 'numeric'){
##           X <- cambiar(as.numeric)
##       }
##   return(X)
##   }

##   df <- df %>% mutate_if( .predicate = buscarLabel, .funs = deleteLabel) 

row.names(df) <- df$id
## creamos el objeto
f1 <- fenoGAIT$new(head(names(df)),
                   g2.ph.labels,
                   df,
                   g2.ph.units,
                   g2.ph.normalRange,
                   g2.ph.machine,
                   g2.ph.formula,
                   g2.ph.description,
                   g2.ph.observations,
                   g2.ph.incompatibilities,
                   g2.ph.covariates,
                   g2.ph.categories,
                   lubridate::today(),
                   g2.ph.map.prot)


## here insert cfDNAmedianRET
## df$id <- as.character(df$id)
## kk <- readxl::read_excel("cfDNAmedianRET.xls")
## kk2 <- kk %>% dplyr::select(Hmisc::Cs(id,cfDNArepOriginal,cfDNAmedianRET)) %>% mutate(id=as.character(id)) %>% as.data.frame 
## df <- df %>% dplyr::left_join(kk2,by='id')

## hacemos la copia de seguridad antes de sobreescrir el archivo
seguridad <- glue::glue("{dataDir}/gait2.phe.all.RData.{format(Sys.time(), '%Y%m%d')}")
file.rename(from = glue::glue("{dataDir}/gait2.phe.all.RData"), to = seguridad)
rm(seguridad)

## creamos el data.frame con las estadisticas de los fenos500
description <- g2.ph.description[fenos500]
units <- g2.ph.units[fenos500]
names(units) <- fenos500
formula <- g2.ph.formula[fenos500]
names(formula) <- fenos500
incompatibilities <- g2.ph.incompatibilities[fenos500]
names(incompatibilities) <- fenos500
machine <- g2.ph.machine[fenos500]
names(machine) <- fenos500
normalRange=g2.ph.normalRange[fenos500]
names(normalRange) <- fenos500
observations=g2.ph.observations[fenos500]
names(observations) <- fenos500
covariate <- sapply(g2.ph.covariates[fenos500], function(X) paste(X, collapse = ','))
covariate[covariate == 'NA'] <- NA_character_
definitions.gait2.500 <- as.data.frame(
    cbind(description,
          units,
          formula,
          incompatibilities,
          machine,
          normalRange,
          observations,
          covariate))
rm(description,
   units,
   formula,
   incompatibilities,
   machine,
   normalRange,
   observations,
   covariate)

definitions.gait2.500$codigo <- row.names(definitions.gait2.500)
definitions.gait2.500$N <- plyr::laply(definitions.gait2.500$codigo, function(X) sum(!is.na(df[,X])))
definitions.gait2.500$h2r <- as.numeric(g2.h2r.fenos500[row.names(definitions.gait2.500), 'h2r'])
definitions.gait2.500$pvalH2R <- as.numeric(g2.h2r.fenos500[row.names(definitions.gait2.500), 'pvalH2R'])

## row.names(df) <- df$id

save(list = ls(), file = glue::glue("{dataDir}/gait2.phe.all.RData"))
save(f1, file = glue::glue("{dataDir}/g2.d.pheno.RData"))




