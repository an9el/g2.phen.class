g2.phen.class
================
:pencil: Angel Martinez-Perez
[![](https://info.orcid.org/wp-content/uploads/2019/11/orcid_24x24.png)](https://orcid.org/0000-0002-5934-1454)





  - [gait2.fenotypes.class](#gait2fenotypesclass)
  - [:arrow\_double\_down: Installation
    <a name=installation></a>](#arrow_double_down-installation-)
  - [:bulb: Purpose <a name=purpose></a>](#bulb-purpose-)
  - [:card\_index: Workflow
    <a name=workflow></a>](#card_index-workflow-)
  - [:newspaper: Gait2 papers
    <a name=papers></a>](#newspaper-gait2-papers-)
  - [:copyright: MIT License
    <a name=license></a>](#copyright-mit-license-)

<!-- README.md is generated from README.Rmd. Please edit that file -->

-----

## gait2.fenotypes.class

<!-- badges: start -->

[![Lifecycle:
superseded](https://img.shields.io/badge/lifecycle-superseded-blue.svg)](https://www.tidyverse.org/lifecycle/#superseded)
[![License:
MIT](https://img.shields.io/badge/license-MIT-blue.svg)](#license)
[![Last-changedate](https://img.shields.io/badge/last%20change-2022--07--28-yellowgreen.svg)](/commits/master)
<!-- badges: end -->

This package is superseded by
[an9elproject](gitlab.com/an9el/an9elproject).

  - :arrow\_double\_down: [Installation](#installation)
  - :bulb: [Purpose](#purpose)
  - :card\_index: [Workflow](#workflow)
  - :newspaper: [Gait2 papers](#papers)

## :arrow\_double\_down: Installation <a name=installation></a>

This is just the definition of the R6 class *fenoGAIT*. It is not a real
R package.

There is also a file **crear.R** to actually build a instance of this
class with the phenotype data in it, but for doing so, you need the
actual data that is not publicly available. Normally you will run the
script that create a new instance of the GAIT2 phenotype object and make
a copy of the old one..

## :bulb: Purpose <a name=purpose></a>

  - source code of the class in a repository
  - source code for create a new instance
  - source code for insert new phenotypes

## :card\_index: Workflow <a name=workflow></a>

Load the required packages

``` r
suppressPackageStartupMessages({
    library(pacman)
    p_load(R6, tidyverse, plyr, Hmisc, labelled, magrittr, stringr)
})
          
```

## :newspaper: Gait2 papers <a name=papers></a>

1.  ([Lopez, Martinez-Perez, Rodriguez-Rius, et al.,
    2022](#bib-Lopez_2022))
2.  ([Kim-Hellmuth, Aguet, Oliva, et al.,
    2020](#bib-Kim-Hellmutheaaz8528))
3.  ([Oliva, Muñoz-Aguirre, Kim-Hellmuth, et al.,
    2020](#bib-Olivaeaba3066))
4.  ([Rodriguez-Rius, Lopez, Martinez-Perez, et al.,
    2020](#bib-Rodriguez-Rius2020a))
5.  ([Rodriguez-Rius, Martinez-Perez, López, et al.,
    2020](#bib-Rodriguez-Rius2020))
6.  ([de Vries, Sabater-Lleal, Huffman, et al., 2019](#bib-Vries2019))
7.  ([Pujol-Moix, Martinez-Perez, Sabater-Lleal, et al.,
    2019](#bib-2019pujol))
8.  ([Sabater-Lleal, Huffman, de Vries, et al.,
    2018](#bib-Sabater-Lleal2018))
9.  ([Ziyatdinov, Vazquez-Santiago, Brunel, et al.,
    2018](#bib-Ziyatdinov2017))
10. ([Martin-Fernandez, Gavidia-Bovadilla, Corrales, et al.,
    2017](#bib-Martin-Fernandez2017))
11. ([Sennblad, Basu, Mazur, et al., 2017](#bib-Sennblad2017))
12. ([Vázquez-Santiago, Ziyatdinov, Pujol-Moix, et al.,
    2016](#bib-VazquezSantiago2016))
13. ([Ziyatdinov, Brunel, Martinez-Perez, et al.,
    2016](#bib-Ziyatdinov2016))
14. ([Hinds, Buil, Ziemek, et al., 2016](#bib-Hinds2016))
15. ([Martin-Fernandez, Ziyatdinov, Carrasco, et al.,
    2016](#bib-MartinFernandez2016))
16. ([Remacha, Vilalta, Sardà, et al., 2016](#bib-Remacha2016))
17. ([Pujol-Moix, Vázquez-Santiago, Morera, et al.,
    2015](#bib-Pujol-Moix2015))
18. ([Camacho, Martinez-Perez, Buil, et al., 2012](#bib-Camacho2012))
19. ([García-Dabrio, Pujol-Moix, Martinez-Perez, et al.,
    2012](#bib-GarciaDabrio2012))
20. ([Athanasiadis, Buil, Souto, et al., 2011](#bib-Athanasiadis2011))
21. ([Sabater-Lleal, Chillón, Mordillo, et al.,
    2010](#bib-Sabater-Lleal2010))
22. ([Vila, Martinez-Perez, Camacho, et al., 2010](#bib-Vila2010))

<details open>

<summary> <span title="Click to Collapse/Expand"> REFERENCES </span>
</summary>

<a name=bib-Lopez_2022></a>[\[1\]](#cite-Lopez_2022) S. Lopez, A.
Martinez-Perez, A. Rodriguez-Rius, et al. “Integrated GWAS and Gene
Expression Suggest ORM1 as a Potential Regulator of Plasma Levels of
Cell-Free DNA and Thrombosis Risk”. In: *Thrombosis and Haemostasis*
(mar. 2022). DOI:
[10.1055/s-0041-1742169](https://doi.org/10.1055%2Fs-0041-1742169). URL:
<http://dx.doi.org/10.1055/s-0041-1742169>.

<a name=bib-Kim-Hellmutheaaz8528></a>[\[2\]](#cite-Kim-Hellmutheaaz8528)
S. Kim-Hellmuth, F. Aguet, M. Oliva, et al. “Cell type-specific genetic
regulation of gene expression across human tissues”. In: *Science*
369.6509 (2020). ISSN: 0036-8075. DOI:
[10.1126/science.aaz8528](https://doi.org/10.1126%2Fscience.aaz8528).
eprint:
<https://science.sciencemag.org/content/369/6509/eaaz8528.full.pdf>.
URL: <https://science.sciencemag.org/content/369/6509/eaaz8528>.

<a name=bib-Olivaeaba3066></a>[\[3\]](#cite-Olivaeaba3066) M. Oliva, M.
Muñoz-Aguirre, S. Kim-Hellmuth, et al. “The impact of sex on gene
expression across human tissues”. In: *Science* 369.6509 (2020). ISSN:
0036-8075. DOI:
[10.1126/science.aba3066](https://doi.org/10.1126%2Fscience.aba3066).
eprint:
<https://science.sciencemag.org/content/369/6509/eaba3066.full.pdf>.
URL: <https://science.sciencemag.org/content/369/6509/eaba3066>.

<a name=bib-Rodriguez-Rius2020a></a>[\[4\]](#cite-Rodriguez-Rius2020a)
A. Rodriguez-Rius, S. Lopez, A. Martinez-Perez, et al. “Identification
of a plasma MicroRNA profile associated with venous thrombosis”. In:
*Arteriosclerosis, Thrombosis, and Vascular Biology* 40.5 (2020), pp.
1392-1399. DOI:
[10.1161/ATVBAHA.120.314092](https://doi.org/10.1161%2FATVBAHA.120.314092).
URL: <https://pubmed.ncbi.nlm.nih.gov/32160777/>.

<a name=bib-Rodriguez-Rius2020></a>[\[5\]](#cite-Rodriguez-Rius2020) A.
Rodriguez-Rius, A. Martinez-Perez, S. López, et al. “Expression of
microRNAs in human platelet-poor plasma: analysis of the factors
affecting their expression and association with proximal genetic
variants.” In: *Epigenetics* (2020). DOI:
[10.1080/15592294.2020.1783497](https://doi.org/10.1080%2F15592294.2020.1783497).
URL: <https://doi.org/10.1080/15592294.2020.1783497>.

<a name=bib-2019pujol></a>[\[6\]](#cite-2019pujol) N. Pujol-Moix, A.
Martinez-Perez, M. Sabater-Lleal, et al. “Influence of ABO Locus on
PFA-100 Collagen-ADP Closure Time Is Not Totally Dependent on the Von
Willebrand Factor. Results of a GWAS on GAIT-2 Project Phenotypes”. In:
*International Journal of Molecular Sciences* 20.13 (jun. 2019), p. 13.
ISSN: 1422-0067. DOI:
[10.3390/ijms20133221](https://doi.org/10.3390%2Fijms20133221). URL:
<https://www.mdpi.com/1422-0067/20/13/3221/pdf>.

<a name=bib-Vries2019></a>[\[7\]](#cite-Vries2019) P. S. de Vries, M.
Sabater-Lleal, J. E. Huffman, et al. “A genome-wide association study
identifies new loci for factor VII and implicates factor VII in ischemic
stroke etiology”. In: *Blood* (2019), pp. blood-2018. DOI:
[10.1182/blood-2018-05-849240](https://doi.org/10.1182%2Fblood-2018-05-849240).
URL: <https://doi.org/10.1182/blood-2018-05-849240>.

<a name=bib-Sabater-Lleal2018></a>[\[8\]](#cite-Sabater-Lleal2018) M.
Sabater-Lleal, J. E. Huffman, P. S. de Vries, et al. “Genome-wide
association trans-ethnic meta-analyses identifies novel associations
regulating coagulation Factor VIII and von Willebrand Factor plasma
levels”. In: *Circulation* (2018). DOI:
[10.1161/circulationaha.118.034532](https://doi.org/10.1161%2Fcirculationaha.118.034532).
URL: <https://doi.org/10.1161/circulationaha.118.034532>.

<a name=bib-Ziyatdinov2017></a>[\[9\]](#cite-Ziyatdinov2017) A.
Ziyatdinov, M. Vazquez-Santiago, H. Brunel, et al. “lme4qtl: linear
mixed models with flexible covariance structure for genetic studies of
related individuals”. In: *BMC Bioinformatics* 19.1 (feb. 2018), p. 68.
ISSN: 1471-2105. DOI:
[10.1186/s12859-018-2057-x](https://doi.org/10.1186%2Fs12859-018-2057-x).
URL: <https://doi.org/10.1186/s12859-018-2057-x>.

<a name=bib-Martin-Fernandez2017></a>[\[10\]](#cite-Martin-Fernandez2017)
L. Martin-Fernandez, G. Gavidia-Bovadilla, I. Corrales, et al. “Next
generation sequencing to dissect the genetic architecture of KNG1 and
F11 loci using factor XI levels as an intermediate phenotype of
thrombosis”. In: *PloS one* 12.4 (2017), p. e0176301. DOI:
[10.1371/journal.pone.0176301](https://doi.org/10.1371%2Fjournal.pone.0176301).
URL:
<https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0176301>.

<a name=bib-Sennblad2017></a>[\[11\]](#cite-Sennblad2017) B. Sennblad,
S. Basu, J. Mazur, et al. “Genome-wide association study with additional
genetic and post-transcriptional analyses reveals novel regulators of
plasma factor XI levels”. In: *Human molecular genetics* 26.3 (2017),
pp. 637-649. DOI:
[10.1093/hmg/ddw401](https://doi.org/10.1093%2Fhmg%2Fddw401). URL:
<https://pubmed.ncbi.nlm.nih.gov/28053049/>.

<a name=bib-Hinds2016></a>[\[12\]](#cite-Hinds2016) D. A. Hinds, A.
Buil, D. Ziemek, et al. “Genome-wide association analysis of
self-reported events in 6135 individuals and 252 827 controls identifies
8 loci associated with thrombosis”. In: *Human molecular genetics* 25.9
(2016), pp. 1867-1874. DOI:
[10.1093/hmg/ddw037](https://doi.org/10.1093%2Fhmg%2Fddw037). URL:
<https://academic.oup.com/hmg/article/25/9/1867/2384901>.

<a name=bib-MartinFernandez2016></a>[\[13\]](#cite-MartinFernandez2016)
L. Martin-Fernandez, A. Ziyatdinov, M. Carrasco, et al. “Genetic
determinants of thrombin generation and their relation to venous
thrombosis: results from the GAIT-2 Project”. In: *PloS one* 11.1
(2016), p. e0146922. DOI:
[10.1371/journal.pone.0146922](https://doi.org/10.1371%2Fjournal.pone.0146922).
URL: <https://pubmed.ncbi.nlm.nih.gov/26784699/>.

<a name=bib-Remacha2016></a>[\[14\]](#cite-Remacha2016) A. F. Remacha,
N. Vilalta, M. P. Sardà, et al. “Erytrocyte-related phenotypes and
genetic susceptibility to thrombosis”. In: *Blood Cells, Molecules, and
Diseases* 59 (2016), pp. 44-48. DOI:
[10.1016/j.bcmd.2016.04.006](https://doi.org/10.1016%2Fj.bcmd.2016.04.006).
URL:
<https://www.sciencedirect.com/science/article/abs/pii/S1079979616300419>.

<a name=bib-VazquezSantiago2016></a>[\[15\]](#cite-VazquezSantiago2016)
M. Vázquez-Santiago, A. Ziyatdinov, N. Pujol-Moix, et al. “Age and
gender effects on 15 platelet phenotypes in a Spanish population”. In:
*Computers in biology and medicine* 69 (2016), pp. 226-233. DOI:
[10.1016/j.compbiomed.2015.12.023](https://doi.org/10.1016%2Fj.compbiomed.2015.12.023).
URL: <https://pubmed.ncbi.nlm.nih.gov/26773944/>.

<a name=bib-Ziyatdinov2016></a>[\[16\]](#cite-Ziyatdinov2016) A.
Ziyatdinov, H. Brunel, A. Martinez-Perez, et al. “solarius: an R
interface to SOLAR for variance component analysis in pedigrees”. In:
*Bioinformatics* 32.12 (2016), pp. 1901-1902. DOI:
[10.1093/bioinformatics/btw080](https://doi.org/10.1093%2Fbioinformatics%2Fbtw080).
URL: <https://pubmed.ncbi.nlm.nih.gov/27153684/>.

<a name=bib-Pujol-Moix2015></a>[\[17\]](#cite-Pujol-Moix2015) N.
Pujol-Moix, M. Vázquez-Santiago, A. Morera, et al. “Genetic determinants
of platelet large-cell ratio, immature platelet fraction, and other
platelet-related phenotypes”. In: *Thrombosis research* 136.2 (2015),
pp. 361-366. DOI:
[10.1016/j.thromres.2015.06.016](https://doi.org/10.1016%2Fj.thromres.2015.06.016).
URL:
<https://www.sciencedirect.com/science/article/abs/pii/S0049384815300311>.

<a name=bib-Camacho2012></a>[\[18\]](#cite-Camacho2012) M. Camacho, A.
Martinez-Perez, A. Buil, et al. “Genetic determinants of 5-lipoxygenase
pathway in a Spanish population and their relationship with
cardiovascular risk”. In: *Atherosclerosis* 224.1 (2012), pp. 129-135.
DOI:
[10.1016/j.atherosclerosis.2012.07.001](https://doi.org/10.1016%2Fj.atherosclerosis.2012.07.001).
URL: <https://pubmed.ncbi.nlm.nih.gov/22835628/>.

<a name=bib-GarciaDabrio2012></a>[\[19\]](#cite-GarciaDabrio2012) M. C.
García-Dabrio, N. Pujol-Moix, Á. Martinez-Perez, et al. “Influence of
age, gender and lifestyle in lymphocyte subsets: report from the Spanish
Gait-2 Study”. In: *Acta haematologica* 127.4 (2012), pp. 244-249. DOI:
[10.1159/000337051](https://doi.org/10.1159%2F000337051). URL:
<https://pubmed.ncbi.nlm.nih.gov/22538526/>.

<a name=bib-Athanasiadis2011></a>[\[20\]](#cite-Athanasiadis2011) G.
Athanasiadis, A. Buil, J. C. Souto, et al. “A genome-wide association
study of the protein C anticoagulant pathway.” Eng. In: *PLoS One* 6.12
(2011), p. e29168. DOI:
[10.1371/journal.pone.0029168](https://doi.org/10.1371%2Fjournal.pone.0029168).
URL: <http://dx.doi.org/10.1371/journal.pone.0029168>.

<a name=bib-Sabater-Lleal2010></a>[\[21\]](#cite-Sabater-Lleal2010) M.
Sabater-Lleal, M. Chillón, C. Mordillo, et al. “Combined cis-regulator
elements as important mechanism affecting FXII plasma levels.” Eng. In:
*Thromb Res* 125.2 (feb. 2010), pp. e55-e60. DOI:
[10.1016/j.thromres.2009.08.019](https://doi.org/10.1016%2Fj.thromres.2009.08.019).
URL: <http://dx.doi.org/10.1016/j.thromres.2009.08.019>.

<a name=bib-Vila2010></a>[\[22\]](#cite-Vila2010) L. Vila, A.
Martinez-Perez, M. Camacho, et al. “Heritability of Thromboxane A 2 and
Prostaglandin E 2 Biosynthetic Machinery in a Spanish Population”. In:
*Arteriosclerosis, thrombosis, and vascular biology* 30.1 (2010), pp.
128-134. DOI:
[10.1161/ATVBAHA.109.193219](https://doi.org/10.1161%2FATVBAHA.109.193219).
URL: <https://pubmed.ncbi.nlm.nih.gov/19850905/>.

</details>

<br>

## :copyright: MIT License <a name=license></a>

> Copyright (c) 2020 Angel Martinez-Perez
> 
> Permission is hereby granted, free of charge, to any person obtaining
> a copy of this software and associated documentation files (the
> “Software”), to deal in the Software without restriction, including
> without limitation the rights to use, copy, modify, merge, publish,
> distribute, sublicense, and/or sell copies of the Software, and to
> permit persons to whom the Software is furnished to do so, subject to
> the following conditions:
> 
> The above copyright notice and this permission notice shall be
> included in all copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
> IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
> CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
> TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
> SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

<details closed>

<summary> <span title="Clik to Expand/Collapse"> Current session info
</span> </summary>

``` r

─ Session info ──────────────────────────────────────
 setting  value
 version  R version 4.2.1 (2022-06-23)
 os       Debian GNU/Linux 11 (bullseye)
 system   x86_64, linux-gnu
 ui       X11
 language en_US:en
 collate  en_US.UTF-8
 ctype    en_US.UTF-8
 tz       Europe/Madrid
 date     2022-07-28
 pandoc   2.9.2.1 @ /usr/bin/ (via rmarkdown)

─ Packages ──────────────────────────────────────────
 package       * version date (UTC) lib source
 assertthat      0.2.1   2019-03-21 [1] CRAN (R 4.2.0)
 backports       1.4.1   2021-12-13 [1] CRAN (R 4.2.0)
 base64enc       0.1-3   2015-07-28 [1] CRAN (R 4.2.0)
 bibtex          0.4.2.3 2020-09-19 [1] CRAN (R 4.2.0)
 broom           1.0.0   2022-07-01 [1] CRAN (R 4.2.1)
 cachem          1.0.6   2021-08-19 [1] CRAN (R 4.2.0)
 callr           3.7.1   2022-07-13 [1] CRAN (R 4.2.1)
 cellranger      1.1.0   2016-07-27 [1] CRAN (R 4.2.0)
 checkmate       2.1.0   2022-04-21 [1] CRAN (R 4.2.0)
 cli             3.3.0   2022-04-25 [1] CRAN (R 4.2.0)
 clipr           0.8.0   2022-02-22 [1] CRAN (R 4.2.0)
 cluster         2.1.3   2022-03-28 [3] CRAN (R 4.2.1)
 colorspace      2.0-3   2022-02-21 [1] CRAN (R 4.2.0)
 crayon          1.5.1   2022-03-26 [1] CRAN (R 4.2.0)
 data.table      1.14.2  2021-09-27 [1] CRAN (R 4.2.0)
 DBI             1.1.3   2022-06-18 [1] CRAN (R 4.2.1)
 dbplyr          2.2.1   2022-06-27 [1] CRAN (R 4.2.1)
 deldir          1.0-6   2021-10-23 [1] CRAN (R 4.2.1)
 desc            1.4.1   2022-03-06 [1] CRAN (R 4.2.0)
 details         0.3.0   2022-03-27 [1] CRAN (R 4.2.0)
 devtools      * 2.4.4   2022-07-20 [1] CRAN (R 4.2.1)
 digest          0.6.29  2021-12-01 [1] CRAN (R 4.2.0)
 dplyr         * 1.0.9   2022-04-28 [1] CRAN (R 4.2.0)
 ellipsis        0.3.2   2021-04-29 [1] CRAN (R 4.2.0)
 evaluate        0.15    2022-02-18 [1] CRAN (R 4.2.0)
 fansi           1.0.3   2022-03-24 [1] CRAN (R 4.2.0)
 fastmap         1.1.0   2021-01-25 [1] CRAN (R 4.2.0)
 forcats       * 0.5.1   2021-01-27 [1] CRAN (R 4.2.0)
 foreign         0.8-82  2022-01-13 [3] CRAN (R 4.2.1)
 Formula       * 1.2-4   2020-10-16 [1] CRAN (R 4.2.0)
 fs              1.5.2   2021-12-08 [1] CRAN (R 4.2.0)
 gargle          1.2.0   2021-07-02 [1] CRAN (R 4.2.0)
 generics        0.1.3   2022-07-05 [1] CRAN (R 4.2.1)
 ggplot2       * 3.3.6   2022-05-03 [1] CRAN (R 4.2.0)
 glue            1.6.2   2022-02-24 [1] CRAN (R 4.2.0)
 googledrive     2.0.0   2021-07-08 [1] CRAN (R 4.2.0)
 googlesheets4   1.0.0   2021-07-21 [1] CRAN (R 4.2.0)
 gridExtra       2.3     2017-09-09 [1] CRAN (R 4.2.0)
 gtable          0.3.0   2019-03-25 [1] CRAN (R 4.2.0)
 haven           2.5.0   2022-04-15 [1] CRAN (R 4.2.0)
 Hmisc         * 4.7-0   2022-04-19 [1] CRAN (R 4.2.0)
 hms             1.1.1   2021-09-26 [1] CRAN (R 4.2.0)
 htmlTable       2.4.1   2022-07-07 [1] CRAN (R 4.2.1)
 htmltools       0.5.3   2022-07-18 [1] CRAN (R 4.2.1)
 htmlwidgets     1.5.4   2021-09-08 [1] CRAN (R 4.2.0)
 httpuv          1.6.5   2022-01-05 [1] CRAN (R 4.2.0)
 httr            1.4.3   2022-05-04 [1] CRAN (R 4.2.0)
 interp          1.1-3   2022-07-13 [1] CRAN (R 4.2.1)
 jpeg            0.1-9   2021-07-24 [1] CRAN (R 4.2.0)
 jsonlite        1.8.0   2022-02-22 [1] CRAN (R 4.2.0)
 knitcitations * 1.0.12  2021-01-10 [1] CRAN (R 4.2.0)
 knitr           1.39    2022-04-26 [1] CRAN (R 4.2.0)
 later           1.3.0   2021-08-18 [1] CRAN (R 4.2.0)
 lattice       * 0.20-45 2021-09-22 [3] CRAN (R 4.2.1)
 latticeExtra    0.6-30  2022-07-04 [1] CRAN (R 4.2.1)
 lifecycle       1.0.1   2021-09-24 [1] CRAN (R 4.2.0)
 lubridate       1.8.0   2021-10-07 [1] CRAN (R 4.2.0)
 magrittr      * 2.0.3   2022-03-30 [1] CRAN (R 4.2.0)
 Matrix          1.4-1   2022-03-23 [3] CRAN (R 4.2.1)
 memoise         2.0.1   2021-11-26 [1] CRAN (R 4.2.0)
 mime            0.12    2021-09-28 [1] CRAN (R 4.2.0)
 miniUI          0.1.1.1 2018-05-18 [1] CRAN (R 4.2.0)
 modelr          0.1.8   2020-05-19 [1] CRAN (R 4.2.0)
 munsell         0.5.0   2018-06-12 [1] CRAN (R 4.2.0)
 nnet            7.3-17  2022-01-13 [1] CRAN (R 4.2.0)
 pillar          1.8.0   2022-07-18 [1] CRAN (R 4.2.1)
 pkgbuild        1.3.1   2021-12-20 [1] CRAN (R 4.2.0)
 pkgconfig       2.0.3   2019-09-22 [1] CRAN (R 4.2.0)
 pkgload         1.3.0   2022-06-27 [1] CRAN (R 4.2.1)
 plyr            1.8.7   2022-03-24 [1] CRAN (R 4.2.0)
 png             0.1-7   2013-12-03 [1] CRAN (R 4.2.0)
 prettyunits     1.1.1   2020-01-24 [1] CRAN (R 4.2.0)
 processx        3.7.0   2022-07-07 [1] CRAN (R 4.2.1)
 profvis         0.3.7   2020-11-02 [1] CRAN (R 4.2.1)
 promises        1.2.0.1 2021-02-11 [1] CRAN (R 4.2.0)
 ps              1.7.1   2022-06-18 [1] CRAN (R 4.2.0)
 purrr         * 0.3.4   2020-04-17 [1] CRAN (R 4.2.0)
 R6              2.5.1   2021-08-19 [1] CRAN (R 4.2.0)
 RColorBrewer    1.1-3   2022-04-03 [1] CRAN (R 4.2.0)
 Rcpp            1.0.9   2022-07-08 [1] CRAN (R 4.2.1)
 readr         * 2.1.2   2022-01-30 [1] CRAN (R 4.2.0)
 readxl          1.4.0   2022-03-28 [1] CRAN (R 4.2.0)
 RefManageR      1.3.0   2020-11-13 [1] CRAN (R 4.2.0)
 remotes         2.4.2   2021-11-30 [1] CRAN (R 4.2.0)
 reprex          2.0.1   2021-08-05 [1] CRAN (R 4.2.0)
 rlang           1.0.4   2022-07-12 [1] CRAN (R 4.2.1)
 rmarkdown     * 2.14    2022-04-25 [1] CRAN (R 4.2.0)
 rpart           4.1.16  2022-01-24 [1] CRAN (R 4.2.0)
 rprojroot       2.0.3   2022-04-02 [1] CRAN (R 4.2.0)
 rstudioapi      0.13    2020-11-12 [1] CRAN (R 4.2.0)
 rvest           1.0.2   2021-10-16 [1] CRAN (R 4.2.0)
 scales          1.2.0   2022-04-13 [1] CRAN (R 4.2.0)
 sessioninfo     1.2.2   2021-12-06 [1] CRAN (R 4.2.0)
 shiny           1.7.2   2022-07-19 [1] CRAN (R 4.2.1)
 stringi         1.7.8   2022-07-11 [1] CRAN (R 4.2.1)
 stringr       * 1.4.0   2019-02-10 [1] CRAN (R 4.2.0)
 survival      * 3.3-1   2022-03-03 [1] CRAN (R 4.2.0)
 tibble        * 3.1.8   2022-07-22 [1] CRAN (R 4.2.1)
 tidyr         * 1.2.0   2022-02-01 [1] CRAN (R 4.2.0)
 tidyselect      1.1.2   2022-02-21 [1] CRAN (R 4.2.0)
 tidyverse     * 1.3.2   2022-07-18 [1] CRAN (R 4.2.1)
 tzdb            0.3.0   2022-03-28 [1] CRAN (R 4.2.0)
 urlchecker      1.0.1   2021-11-30 [1] CRAN (R 4.2.1)
 usethis       * 2.1.6   2022-05-25 [1] CRAN (R 4.2.0)
 utf8            1.2.2   2021-07-24 [1] CRAN (R 4.2.0)
 vctrs           0.4.1   2022-04-13 [1] CRAN (R 4.2.0)
 withr           2.5.0   2022-03-03 [1] CRAN (R 4.2.0)
 xfun            0.31    2022-05-10 [1] CRAN (R 4.2.0)
 xml2            1.3.3   2021-11-30 [1] CRAN (R 4.2.0)
 xtable          1.8-4   2019-04-21 [1] CRAN (R 4.2.0)
 yaml            2.3.5   2022-02-21 [1] CRAN (R 4.2.0)

 [1] /usr/local/lib/R/site-library
 [2] /usr/lib/R/site-library
 [3] /usr/lib/R/library

─────────────────────────────────────────────────────
```

</details>

<br>
